# by Chinbat
# 2015/11/23

import os
import json
import struct
import logging
from time import sleep

import wiringpi2
from consts import PUBKEY, SUBKEY, log_path, channel_name
from pubnub import Pubnub

# config
logging.basicConfig(filename=log_path, format='%(asctime)s %(message)s')
pubnub = Pubnub(publish_key=PUBKEY, subscribe_key=SUBKEY)
wiringpi2.wiringPiSetup()
i2c = wiringpi2.I2C()

def callback(message):
  logging.info(message)

def round_value(value):
  return float("{:.2f}".format(value))

def index_to_message(index):
  if index < 55:
    return "It's very cold!"
  elif index < 60:
    return "It's cold"
  elif index < 65:
    return "Not so cold"
  elif index < 70:
    return "Comfortable!"
  elif index < 75:
    return "Not so hot"
  elif index < 80:
    return "It's hot"
  elif index < 85:
    return "It's very hot"
  else:
    return "It's too hot to be alive!"

def is_comfortable(index):
  if index >= 60 and index < 75:
    return 1
  else:
    return 0

# setup
dev = i2c.setup(0x40)
i2c.writeReg16(dev,0x02,0x10) # Temp + Hidi 32-bit transfer mode, LSB-MSB invertion
i2c.writeReg8(dev,0x00,0x00) # Start conversion
sleep((6350.0 + 6500.0 +  500.0)/1000000.0) # Wait for conversion

# get data from sensor
raw_temp = ((struct.unpack('4B', os.read(dev,4)))[0] << 8 | (struct.unpack('4B', os.read(dev,4)))[1])
raw_humi = ((struct.unpack('4B', os.read(dev,4)))[2] << 8 | (struct.unpack('4B', os.read(dev,4)))[3])
os.close(dev) # Don't leave the door open.

# after process
temp_pre = (raw_temp/65535.0)*165-40
humi_pre = (raw_humi/65535.0)*100
index_pre = 0.81*temp_pre + 0.01*humi_pre*(0.99*temp_pre-14.3)+46.3

# create pubnub data
temp, humi, index = map(round_value, [temp_pre, humi_pre, index_pre])
data_dict = {"temp":temp, "humi":humi, "index":index, "message":index_to_message(index), "is_comfortable": is_comfortable(index)}
data = json.dumps(data_dict)

# pubnub
pubnub.publish(channel_name, data, callback=callback, error=callback)
logging.info(data)
